package org.example.repository;

import org.example.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long> {
    User findByUsername(String username);

    @Query("SELECT u FROM User u WHERE CONCAT(u.id,' ', u.username,' ', u.email,' ',u.active) LIKE %?1%")
    Page<User> findAll(String keyword, Pageable pageable);
    List<User> findAll();
    User findByActivationCode(String code);
    List<User> findAllByCurrentWorker(Long currentWorker);
    List<User> findByCurrentWorkerIsNullAndActiveTrueAndRegisteredUserTrue();

}
