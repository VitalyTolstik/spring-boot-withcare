package org.example.repository;

import org.example.domain.Message;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message, Long> {
    List<Message> findAllByAuthor_Id(Long id);
    List<Message> findByWasReadFalse();
}
