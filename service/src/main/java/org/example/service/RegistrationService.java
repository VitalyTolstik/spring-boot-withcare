package org.example.service;

import org.example.domain.User;
import org.springframework.stereotype.Service;

@Service
public interface RegistrationService {
    User findByUsername(User user);
    void saveUser(User user);
    boolean activateUser(String code);
}
