package org.example.service;

import org.example.domain.Role;
import org.example.domain.User;
import org.example.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Collections;
import java.util.UUID;

@Service
public class RegistrationServiceImpl implements RegistrationService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MailSenderServiceImpl mailSenderServiceImpl;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public User findByUsername(User user) {
        return userRepository.findByUsername(user.getUsername());
    }

    public void saveUser(User user) {

        User userFromDb = userRepository.findByUsername(user.getUsername());

        if (userFromDb != null) {
            return;
        }
        user.setActive(false);
        user.setRegisteredUser(true);
        user.setRole(Collections.singletonList(Role.ROLE_USER));
        user.setActivationCode(UUID.randomUUID().toString());
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        userRepository.save(user);

        if (!StringUtils.isEmpty(user.getEmail())) {
            String message = String.format(
                    "Hello, %s! \n" +
                            "Welcome to WithCare. Please, visit next link: http://localhost:8081/activate/%s",
                    user.getUsername(),
                    user.getActivationCode()
            );

            mailSenderServiceImpl.send(user.getEmail(), "Activation code", message);
        }

    }

    public boolean activateUser(String code) {
        User user = userRepository.findByActivationCode(code);

        if (user == null) {
            return false;
        }

        user.setActivationCode(null);
        user.setActive(true);
        userRepository.save(user);

        return true;
    }
}
