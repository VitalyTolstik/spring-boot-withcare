package org.example.service;

import org.example.domain.Message;
import org.example.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface AdminService {
    Page<User> listAll(int pageNumber, String sortField, String sortDir, String keyword);
    void saveWithEncoding(User user);
    User findById(Long id);
    void delete(User user);
    Integer findCountNewUserWithoutWorker();
    List<User> findWorker();
    List<User> findUser();
    void save(User user);
    List<User> findAllExactUser(Long id);
    List<Message> getMessage(Long id);
    Integer findCountNewMessage(Long id);
}
