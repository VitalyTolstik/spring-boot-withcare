package org.example.service;

import org.example.domain.Message;
import org.example.domain.Role;
import org.example.domain.User;
import org.example.repository.MessageRepository;
import org.example.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AdminServiceImpl implements UserDetailsService, AdminService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username + " exist!");
        } else return user;
    }

    public Page<User> listAll(int pageNumber, String sortField, String sortDir, String keyword) {
        Sort sort = Sort.by(sortField);
        sort = sortDir.equals("asc") ? sort.ascending() : sort.descending();
        Pageable pageable = PageRequest.of(pageNumber - 1, 10, sort);
        if (keyword != null) {
            return userRepository.findAll(keyword, pageable);
        }
        return userRepository.findAll(pageable);
    }

    public void saveWithEncoding(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }

    public void save(User user) {
        userRepository.save(user);
    }

    public User findById(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
    }

    public void delete(User user) {
        userRepository.delete(user);
    }

    public Integer findCountNewUserWithoutWorker() {
        final List<User> byCurrentWorkerIsNull = userRepository.findByCurrentWorkerIsNullAndActiveTrueAndRegisteredUserTrue();
        return byCurrentWorkerIsNull.size();
    }

    public List<User> findWorker() {
        final List<User> all = userRepository.findAll();
        List<User> workerList = new ArrayList<>();
        for (User user : all) {
            if (user.getRole().contains(Role.ROLE_WORKER))
                workerList.add(user);
        }
        return workerList;
    }

    public List<User> findUser() {
        final List<User> all = userRepository.findAll();
        List<User> userList = new ArrayList<>();
        for (User user : all) {
            if (user.getRole().contains(Role.ROLE_USER))
                userList.add(user);
        }
        return userList;
    }

    public List<User> findAllExactUser(Long id) {
        return userRepository.findAllByCurrentWorker(id);
    }

    public Integer findCountNewMessage(Long id) {
        int count = 0;
        final List<User> allByCurrentWorker = userRepository.findAllByCurrentWorker(id);
        for (User user : allByCurrentWorker) {
            Integer unreadCount = user.getUnreadCount();
            if (unreadCount != null) count+=unreadCount;
        }
        return count;
    }

    public List<Message> getMessage(Long id) {
        final List<User> allByCurrentWorker = userRepository.findAllByCurrentWorker(id);
        final List<Message> allMessage = messageRepository.findAll();
        List<Message> copyAllMessage = new ArrayList<>();

        for (User user : allByCurrentWorker) {
            for (Message message : allMessage) {
                if (user.getId().equals(message.getAuthor().getId())) {
                    copyAllMessage.add(message);
                }
            }
        }
        return copyAllMessage;
    }
}
