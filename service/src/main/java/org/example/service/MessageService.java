package org.example.service;

import org.example.domain.Message;
import org.example.domain.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface MessageService {
    List<Message> findAllMessageOfCurrentUser(Long id);
    Integer countUnreadMessage();
    List<Message> getAllUnreadMessage();
    Message newMessage(String text, User user);
    List<Message> getAllMessageChosenUser(Long id);
}
