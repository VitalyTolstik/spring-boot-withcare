package org.example.service;

import org.springframework.stereotype.Service;

@Service
public interface MailSenderService {
    void send(String emailTo, String subject, String message);
}
