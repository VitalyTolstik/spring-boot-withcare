package org.example.service;

import org.example.domain.Message;
import org.example.domain.User;
import org.example.repository.MessageRepository;
import org.example.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class MessageServiceImpl implements MessageService {

    @Autowired
    private MessageRepository messageRepository;
    @Autowired
    private UserRepository userRepository;

    public List<Message> findAllMessageOfCurrentUser(Long id) {
        return messageRepository.findAllByAuthor_Id(id);
    }

    public Integer countUnreadMessage() {
        final List<Message> byWasReadFalse = messageRepository.findByWasReadFalse();
        return byWasReadFalse.size();
    }
    public List<Message> getAllUnreadMessage() {
        return messageRepository.findByWasReadFalse();
    }

    public Message newMessage(String text, User user) {
        Message message = new Message(text, user, LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss")), false);
        Integer unreadCount = user.getUnreadCount();
        if (unreadCount == null) unreadCount = 0;
        user.setUnreadCount(++unreadCount);
        userRepository.save(user);
        messageRepository.save(message);
        return message;
    }

    public List<Message> getAllMessageChosenUser(Long id) {
        final List<Message> allMessageOfCurrentUser = messageRepository.findAllByAuthor_Id(id);
        for (Message message : allMessageOfCurrentUser) {
            message.setWasRead(true);
            message.getAuthor().setUnreadCount(null);
            messageRepository.save(message);
        }
        return allMessageOfCurrentUser;
    }
}
