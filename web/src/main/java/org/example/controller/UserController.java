package org.example.controller;

import org.example.domain.Message;
import org.example.domain.User;
import org.example.service.AdminService;
import org.example.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class UserController {

    @Autowired
    private MessageService messageServiceImpl;

    @Autowired
    private AdminService adminServiceImpl;

    @GetMapping("/hello")
    public String view(@AuthenticationPrincipal User user, Model model) {

        if (user.getCurrentWorker() != null) {
            final User byId = adminServiceImpl.findById(user.getCurrentWorker());
            model.addAttribute("currentWorker", "Your personal manager is " + byId.getUsername());
        } else {
            model.addAttribute("currentWorker", "You don't have personal manager");
        }
        model.addAttribute("count_new_message", adminServiceImpl.findCountNewMessage(user.getId()));
        model.addAttribute("list_user_for_worker", adminServiceImpl.findAllExactUser(user.getId()));
        model.addAttribute("list_message_for_worker", adminServiceImpl.getMessage(user.getId()));
        model.addAttribute("count_user", adminServiceImpl.findCountNewUserWithoutWorker());
        model.addAttribute("count_unread_message", messageServiceImpl.countUnreadMessage());
        model.addAttribute("message", new Message());
        model.addAttribute("messages", messageServiceImpl.findAllMessageOfCurrentUser(user.getId()));
        return "hello";
    }

    @PostMapping("/hello")
    public String add(
            @RequestParam String text,
            @AuthenticationPrincipal User user,
            Model model) {

        model.addAttribute("message", messageServiceImpl.newMessage(text, user));
        model.addAttribute("messages", messageServiceImpl.findAllMessageOfCurrentUser(user.getId()));

        return "redirect:/hello";
    }




}
