package org.example.controller;

import org.example.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MessageController {

    @Autowired
    private MessageService messageServiceImpl;

    @GetMapping("/chat")
    public String getListNewMessageOfWorker(@RequestParam ("contactIdToChat") Long id, Model model) {
        model.addAttribute("messages", messageServiceImpl.getAllMessageChosenUser(id));
        return "new_message_worker";
    }

}
