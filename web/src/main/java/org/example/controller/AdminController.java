package org.example.controller;

import org.example.domain.User;
import org.example.service.AdminService;
import org.example.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private AdminService adminServiceImpl;

    @Autowired
    private MessageService messageService;

    @GetMapping
    public String viewHomePage(Model model) {
        String keyword = null;
        return listByPage(1, model, "id", "asc", keyword);
    }

    @GetMapping("/page/{pageNumber}")
    public String listByPage(@PathVariable("pageNumber") int currentPage,
                             Model model,
                             @Param("sortField") String sortField,
                             @Param("sortDir") String sortDir,
                             @Param("keyword") String keyword) {

        final Page<User> page = adminServiceImpl.listAll(currentPage, sortField, sortDir, keyword);
        final long totalElements = page.getTotalElements();
        final int totalPages = page.getTotalPages();
        final List<User> users = page.getContent();

        model.addAttribute("currentPage", currentPage);
        model.addAttribute("totalElements", totalElements);
        model.addAttribute("totalPages", totalPages);
        model.addAttribute("users", users);
        model.addAttribute("sortField", sortField);
        model.addAttribute("sortDir", sortDir);
        model.addAttribute("keyword", keyword);

        String reverseSortDir = sortDir.equals("asc") ? "desc" : "asc";
        model.addAttribute("reverseSortDir",reverseSortDir);
        return "admin_page";
    }

    @GetMapping("/add")
    public String showSignUpForm(Model model) {
        model.addAttribute("user", new User());
        return "add_user";
    }


    @PostMapping("/users/add")
    public String addUser(@Valid @ModelAttribute("user") User user,
                          BindingResult result,
                          @RequestParam ("contactIdToEdit") Long id) {

        if (result.hasErrors()) {
            return "add_user";
        }

        if (id != null) {
            User user1 = adminServiceImpl.findById(id);
            user.setId(user1.getId());
        }

        adminServiceImpl.saveWithEncoding(user);
        return "redirect:/admin";
    }

    @PostMapping("/users/edit")
    public String showUpdateForm(@RequestParam ("contactIdToEdit") Long id, Model model) {
        User user = adminServiceImpl.findById(id);
        model.addAttribute("user", user);
        return "add_user";
    }

    @GetMapping("/users/delete")
    public String deleteUser(@RequestParam ("contactIdToDelete") Long id) {
        User user = adminServiceImpl.findById(id);
        adminServiceImpl.delete(user);
        return "redirect:/admin";
    }

    @GetMapping("/new_message")
    public String getListNewMessage(Model model) {
        model.addAttribute("all_unread_message", messageService.getAllUnreadMessage());
        return "new_message";
    }

    @GetMapping("/new_user")
    public String getListNewUserWithoutWorker(@ModelAttribute User user, Model model) {
        model.addAttribute("user", user);
        model.addAttribute("all_user", adminServiceImpl.findUser());
        model.addAttribute("workers", adminServiceImpl.findWorker());
        return "new_user_without_worker";
    }

    @PostMapping("/new_user/set")
    public String showSetForm(@RequestParam ("contactIdToEdit") Long id, Model model) {
        User user = adminServiceImpl.findById(id);
        model.addAttribute("workers", adminServiceImpl.findWorker());
        model.addAttribute("user", user);
        return "set_worker";
    }

    @PostMapping("/new_user/add")
    public String addUser(@RequestParam ("contactIdToEdit") Long id, @RequestParam ("currentWorker") Long currentWorker) {
        User user = adminServiceImpl.findById(id);
        user.setCurrentWorker(currentWorker);

        adminServiceImpl.save(user);
        return "redirect:/admin/new_user";
    }
}
